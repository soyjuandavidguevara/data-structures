import {Map} from "../ADT/Map";
import {List} from "../ADT/List";
import {MapEntry} from "./MapEntry";
import {ArrayList} from "./ArrayList";
import {LinkedList} from "./LinkedList";

export class HashMap<K, V> extends Map<K, V> {
    private prime: number;
    private capacity: number;
    private scale: number;
    private shift: number;
    private table: List<List<MapEntry<K, V>>>;
    private limitFactor: number;
    private currentFactor: number;
    private size: number;

    constructor(numOfElements: number = 17, prime: number = 109345121, loadFactor: number = 0.5, compareFunction?: (e1: K, e2: MapEntry<K, V>) => number) {
        super(compareFunction);
        this.capacity = HashMap.nextPrime(numOfElements/loadFactor);
        this.prime = prime;
        this.scale = HashMap.randomInt(1, prime - 1);
        this.shift = HashMap.randomInt(0, prime - 1);
        this.limitFactor = loadFactor;
        this.currentFactor = 0;
        this.table = new ArrayList<List<MapEntry<K, V>>>();
        this.size = 0;

        for (let i = 0; i < this.capacity; i++) {
            const bucket = new LinkedList<MapEntry<K, V>>();
            this.table.addLast(bucket);
        }
    }

    public getSize(): number {
        return this.size;
    }

    public contains(key: K): boolean {
        const position = this.hashValue(key);
        const bucket = this.table.getElement(position);
        const pos = bucket.isPresent(key);
        if (pos >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public put(key: K, val: V): void {
        const hash = this.hashValue(key);
        const bucket = this.table.getElement(hash);
        const entry = new MapEntry(key, val);
        const pos = bucket.isPresent(key);

        if (pos >= 0) {
            bucket.changeInfo(entry, pos);
        } else {
            bucket.addLast(entry);
            this.size += 1;
            this.currentFactor = this.size / this.capacity;

            if (this.currentFactor >= this.limitFactor) {
                this.rehash();
            }
        }
    }

    public get(key: K): MapEntry<K, V> | null {
        const hash = this.hashValue(key);

        const bucket = this.table.getElement(hash);

        if (bucket.size() > 0) {
            bucket.forEach((e) => {
                if (this.compareFunction(key, e) === 0) {
                    return e;
                } else {
                    console.log(e);
                }
            })
        }

        return null;
    }

    private hashValue(key: K) {
        const h = this.hash((key as any).toString());
        const a = this.scale;
        const b = this.shift;
        const p = this.prime;
        const m = this.capacity;
        return Math.floor((Math.abs(a * h + b) % p) % m);
    }

    private static isPrime(n: number): boolean {
        if (n <= 1) return false;

        if (n <= 3) return true;

        if (n % 2 == 0 || n % 3 == 0) return false;

        for (let i = 5; i < Math.floor(Math.sqrt(n) + 1); i += 6) {
            if (n % i == 0 || n % (i + 2) == 0) return false;
        }

        return true;
    }

    private static randomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private static nextPrime(n: number): number {
        if (n <= 1) {
            return 2;
        }

        let prime = Math.floor(n);
        let found = false;

        while (!found) {
            prime += 1
            if (HashMap.isPrime(prime)) {
                found = true;
            }
        }

        return prime;
    }

    private rehash() : void {
        const newTable = new ArrayList<List<MapEntry<K, V>>>(this.compareFunction);
        const capacity = HashMap.nextPrime(this.capacity * 2);
        const oldTable = this.table;

        this.size = 0;
        this.currentFactor = 0;
        this.table = newTable;
        this.capacity = capacity;

        oldTable.forEach((e) => {
            if (e.size() > 0) {
                e.forEach((entry) => {
                    this.put(entry.getKey(), entry.getValue());
                })
            }
        })
    }
}