import { List } from "../ADT/List";
import { LinkedNode } from "./LinkedNode";
import {MapEntry} from "./MapEntry";

export class LinkedList<Type> extends List<Type> {
  private first: LinkedNode<Type> | undefined;
  private last: LinkedNode<Type> | undefined;
  private listSize: number;

  constructor(compareFunction?: (e1: any, e2: any) => number) {
    super(compareFunction);

    this.first = undefined;
    this.last = undefined;
    this.listSize = 0;
  }

  addElement(element: Type, position: number): void {
    if (position > this.size() || position < 0) {
      throw new Error("Array indices out of range");
    } else if (position === 0) {
      this.addFirst(element);
    } else if (position === this.size()) {
      this.addLast(element);
    } else {
      let currentPos = 0;
      let currentNode = this.first;
      let nextNode = this.first;
      while (currentPos < position) {
        currentNode = nextNode;
        nextNode = currentNode?.getNext();
        currentPos++;
      }

      const newNode = new LinkedNode<Type>(element);
      newNode.setNext(nextNode);
      currentNode?.setNext(newNode);
    }
    this.listSize += 1;
  }

  addFirst(element: Type): void {
    const newNode = new LinkedNode<Type>(element);
    if (this.first) {
      newNode.setNext(this.first);
      this.first = newNode;
    } else {
      this.first = newNode;
      this.last = newNode;
    }
    this.listSize += 1;
  }

  addLast(element: Type): void {
    const newNode = new LinkedNode<Type>(element);
    if (this.first) {
      this.last?.setNext(newNode);
      this.last = newNode;
    } else {
      this.first = newNode;
      this.last = newNode;
    }
    this.listSize += 1;
  }

  changeInfo(element: Type, position: number): Type {
    let currentPos = 0;
    let prevNode = this.first;
    let currentNode = this.first;
    while (currentPos < position) {
      prevNode = currentNode;
      currentNode = currentNode?.getNext();
      currentPos++;
    }
    currentNode?.setValue(element);
    return element;
  }

  deleteElement(position: number): Type {
    let current;
    if (position > this.size() || position < 0) {
      throw new Error("Array indices out of range");
    } else if (position === 0) {
      return this.deleteFirst();
    } else if (position === this.size()) {
      return this.deleteLast();
    } else {
      let currentPos = 0;
      let prev = this.first;
      let currentNode = this.first;
      while (currentPos < position) {
        prev = currentNode;
        currentNode = currentNode?.getNext();
        currentPos++;
      }

      current = currentNode?.getValue();
      prev?.setNext(currentNode?.getNext());
    }
    this.listSize -= 1;
    return current as Type;
  }

  deleteFirst(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    } else if (this.size() === 1) {
      const elem = this.first?.getValue();
      this.first = undefined;
      this.last = undefined;
      this.listSize -= 1;
      return elem as Type;
    } else {
      const elem = this.first?.getValue();
      this.first = this.first?.getNext();
      this.listSize -= 1;
      return elem as Type;
    }
  }

  deleteLast(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    } else if (this.size() === 1) {
      const elem = this.first?.getValue();
      this.first = undefined;
      this.last = undefined;
      this.listSize -= 1;
      return elem as Type;
    } else {
      let prev = this.first;
      let currentNode = this.first;
      while (currentNode?.hasNext()) {
        prev = currentNode;
        currentNode = currentNode?.getNext();
      }
      prev?.setNext(undefined);
      this.last = prev;
      this.listSize -= 1;
      return currentNode?.getValue() as Type;
    }
  }

  firstElement(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    }
    return this.first?.getValue() as Type;
  }

  forEach(callback: (e: Type) => any): void {
    if (this.isEmpty()) {
      return;
    } else if (this.size() === 1) {
      const elem = this.first?.getValue();
      callback(elem as Type);
    } else {
      let currentNode = this.first;
      while (currentNode?.hasNext()) {
        callback(currentNode?.getValue());
        currentNode = currentNode?.getNext();
      }
      callback(currentNode?.getValue() as Type);
    }
  }

  getElement(position: number): Type {
    let current;
    if (position > this.size() || position < 0) {
      throw new Error("Array indices out of range");
    } else if (position === 0) {
      return this.firstElement();
    } else if (position === this.size()) {
      return this.lastElement();
    } else {
      let currentPos = 0;
      let prev = this.first;
      let currentNode = this.first;
      while (currentPos < position) {
        prev = currentNode;
        currentNode = currentNode?.getNext();
        currentPos++;
      }
      current = currentNode?.getValue();
    }
    return current as Type;
  }

  isEmpty(): boolean {
    return this.size() === 0;
  }

  lastElement(): Type {
    return this.last?.getValue() as Type;
  }

  size(): number {
    return this.listSize;
  }

  subList(position: number, numberElements: number): List<Type> {
    const sublist = new LinkedList<Type>(this.compareFunction);

    let currentPos = 0;
    let prevNode = this.first;
    let currentNode = this.first;
    while (currentPos < position) {
      prevNode = currentNode;
      currentNode = currentNode?.getNext();
      currentPos++;
    }

    let count = 0;
    while (count < numberElements) {
      sublist.addLast(currentNode?.getValue() as Type);
      currentNode = currentNode?.getNext();
      count += 1;
    }

    return sublist;
  }

  isPresent(element: Type): number {
    let currentPos = 0;
    let position = -1;

    this.forEach((e) => {
      if (this.compareElements(element, e) === 0) {
        position = currentPos;
      }
      currentPos ++;
    });

    return position;
  }

  private compareElements(element: Type, info: Type): number {
    if (info instanceof MapEntry) {
      return this.compareFunction((info as MapEntry<any, any>).getKey(), element);
    } else {
      return this.compareFunction(element, info);
    }
  }
}
