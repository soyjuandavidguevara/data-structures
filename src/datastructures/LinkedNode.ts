export class LinkedNode<Type> {
  private next: LinkedNode<Type> | undefined;
  private value: Type;

  constructor(value: Type) {
    this.value = value;
  }

  getNext(): LinkedNode<Type> | undefined {
    return this.next;
  }

  setNext(value: LinkedNode<Type> | undefined): void {
    this.next = value;
  }

  getValue(): Type {
    return this.value;
  }

  setValue(value: Type): void {
    this.value = value;
  }

  hasNext(): boolean {
    return this.next !== undefined;
  }
}
