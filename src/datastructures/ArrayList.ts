import { List } from "../ADT/List";
import {MapEntry} from "./MapEntry";

export class ArrayList<Type> extends List<Type> {
  private readonly elements: Type[];

  constructor(compareFunction?: (e1: any, e2: any) => number) {
    super(compareFunction);
    this.elements = [];
  }

  firstElement(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    }
    return this.elements[0];
  }

  lastElement(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    }
    return this.elements[this.size() - 1];
  }

  getElement(position: number): Type {
    if (position > this.size() || position < 0) {
      throw new Error("Array indices out of range");
    }
    return this.elements[position];
  }

  addFirst(element: Type): void {
    this.elements.unshift(element);
  }

  addLast(element: Type): void {
    this.elements.push(element);
  }

  addElement(element: Type, position: number): void {
    if (position > this.size() || position < 0) {
      throw new Error("Array indices out of range");
    } else if (position === 0) {
      this.addFirst(element);
    } else if (position === this.size()) {
      this.addLast(element);
    } else {
      this.elements.splice(position, 0, element);
    }
  }

  deleteFirst(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    } else {
      return this.elements.shift() as Type;
    }
  }

  deleteLast(): Type {
    if (this.isEmpty()) {
      throw new Error("Array indices out of range");
    } else {
      return this.elements.pop() as Type;
    }
  }

  deleteElement(position: number): Type {
    const element = this.elements[position];
    this.elements.splice(position, 1);
    return element;
  }

  changeInfo(element: Type, position: number): Type {
    if (position >= this.size() || position < 0) {
      throw new Error("Array indices out of range");
    }
    this.elements[position] = element;
    return element;
  }

  forEach(callback: (e: Type) => any): void {
    this.elements.forEach((e) => callback(e));
  }

  isEmpty(): boolean {
    return this.size() === 0;
  }

  size(): number {
    return this.elements.length;
  }

  subList(position: number, numberElements: number): List<Type> {
    const sublist: List<Type> = new ArrayList(this.compareFunction);

    for (let i = position; sublist.size() < numberElements; i++) {
      sublist.addLast(this.getElement(i));
    }

    return sublist;
  }

  isPresent(element: Type): number {
    let currentPos = 0;
    let position = -1;

    this.forEach((e) => {
      if (this.compareElements(element, e) === 0) {
        position = currentPos;
      }
      currentPos ++;
    });

    return position;
  }

  private compareElements(element: Type, info: Type): number {
    if (info instanceof MapEntry) {
      return this.compareFunction((info as MapEntry<any, any>).getKey(), element);
    } else {
      return this.compareFunction(element, info);
    }
  }
}
