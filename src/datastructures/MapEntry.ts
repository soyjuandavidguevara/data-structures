export class MapEntry<K, V> {
    private readonly value: V;
    private readonly key: K;

    constructor(key: K, value: V) {
        this.value = value;
        this.key = key;
    }

    getKey(): K {
        return this.key;
    }

    getValue(): V {
        return this.value;
    }
}