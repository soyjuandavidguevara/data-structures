import { List } from "../../ADT/List";

function selectionSort(list: List<any>): List<any> {
  let k = 0;
  while (k < list.size()) {
    for (let i = k; i < list.size(); i++) {
      const elem = list.getElement(i);
      const current = list.getElement(k);
      if (list.compareFunction(elem, current) === -1) {
        list.exchangeInfo(i, k);
      }
    }
    k += 1;
  }

  return list;
}

export { selectionSort };
