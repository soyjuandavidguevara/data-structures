import { List } from "../../ADT/List";

function mergeSort(list: List<any>): List<any> {
  if (list.size() <= 1) {
    return list;
  }

  const mid = Math.floor(list.size() / 2);

  const left = list.subList(0, mid);
  const right = list.subList(mid, list.size() - mid);

  mergeSort(left);
  mergeSort(right);

  let i = 0;
  let j = 0;
  let k = 0;

  const leftSize = left.size();
  const rightSize = right.size();

  while (i < leftSize && j < rightSize) {
    const elemLeft = left.getElement(i);
    const elemRight = right.getElement(j);

    if (list.compareFunction(elemRight, elemLeft) === -1) {
      list.changeInfo(elemRight, k);
      j += 1;
    } else {
      list.changeInfo(elemLeft, k);
      i += 1;
    }
    k += 1;
  }

  while (i < leftSize) {
    list.changeInfo(left.getElement(i), k);
    i += 1;
    k += 1;
  }

  while (j < rightSize) {
    list.changeInfo(right.getElement(j), k);
    j += 1;
    k += 1;
  }

  return list;
}

export { mergeSort };
