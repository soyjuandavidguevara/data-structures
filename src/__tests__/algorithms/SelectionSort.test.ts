import { ArrayList } from "../../datastructures/ArrayList";
import { LinkedList } from "../../datastructures/LinkedList";
import { selectionSort } from "../../algorithms/Sorting/SelectionSort";

describe("SelectionSort", () => {
  it("Should sort correctly an ArrayList", () => {
    const array = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      array.addFirst(i);
    }

    selectionSort(array);

    expect(array.firstElement()).toBe(0);
    expect(array.lastElement()).toBe(9);
  });

  it("Should sort correctly a LinkedList", () => {
    const linked = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      linked.addFirst(i);
    }

    selectionSort(linked);

    expect(linked.firstElement()).toBe(0);
    expect(linked.lastElement()).toBe(9);
  });

  it("Should sort correctly with custom compare function", () => {
    function myCompareFunction(e1: number, e2: number): number {
      if (e1 == e2) {
        return 0;
      } else if (e1 < e2) {
        return 1;
      } else {
        return -1;
      }
    }

    const linked = new LinkedList<number>(myCompareFunction);

    for (let i = 0; i < 10; i++) {
      linked.addLast(i);
    }

    selectionSort(linked);

    expect(linked.firstElement()).toBe(9);
    expect(linked.lastElement()).toBe(0);
  });
});
