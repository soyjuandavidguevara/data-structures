import { ArrayList } from "../../datastructures/ArrayList";

describe("ArrayList", () => {
  it("Add elements should work (no position validation)", () => {
    const builtArrayToComparison = [];
    const dummyArrayList = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      builtArrayToComparison.push(i);
      dummyArrayList.addLast(i);
    }

    expect(builtArrayToComparison.length).toBe(dummyArrayList.size());
  });

  it("Getting elements on ArrayList should work properly", () => {
    const dummyArrayList = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      dummyArrayList.addLast(i);
    }

    expect(dummyArrayList.getElement(5)).toBe(5);
    expect(dummyArrayList.firstElement()).toBe(0);
    expect(dummyArrayList.lastElement()).toBe(9);
    expect(() => {
      dummyArrayList.getElement(45);
    }).toThrow("Array indices out of range");

    const emptyArray = new ArrayList();

    expect(() => {
      emptyArray.firstElement();
    }).toThrow("Array indices out of range");
  });

  it("Adding elements on ArrayList should work properly", () => {
    const dummyArrayList = new ArrayList<any>();

    expect(() => {
      dummyArrayList.addElement("", 5);
    }).toThrow("Array indices out of range");

    dummyArrayList.addElement("one", 0);
    expect(dummyArrayList.firstElement()).toBe("one");

    dummyArrayList.addElement("two", 0);
    expect(dummyArrayList.firstElement()).toBe("two");

    dummyArrayList.addElement("three", 0);
    expect(dummyArrayList.firstElement()).toBe("three");

    expect(dummyArrayList.lastElement()).toBe("one");

    dummyArrayList.addElement("2.5", 1);

    expect(dummyArrayList.getElement(1)).toBe("2.5");
  });

  it("Deleting elements on ArrayList should work properly", () => {
    const dummyArrayList = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      dummyArrayList.addLast(i);
    }

    expect(dummyArrayList.deleteElement(5)).toBe(5);
    expect(dummyArrayList.size()).toBe(9);
    expect(dummyArrayList.deleteFirst()).toBe(0);
    expect(dummyArrayList.size()).toBe(8);
    expect(dummyArrayList.deleteLast()).toBe(9);
    expect(dummyArrayList.size()).toBe(7);
    expect(dummyArrayList.lastElement()).toBe(8);
  });

  it("ForEach function on ArrayList should work properly", () => {
    const dummyArrayList = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      dummyArrayList.addLast(i);
    }

    let count = 0;
    let dummyString = "";

    dummyArrayList.forEach((e: any) => {
      count += e;
      dummyString += e.toString();
    });

    expect(count).toBe(45);
    expect(dummyString).toBe("0123456789");
  });

  it("SubList function on ArrayList should work properly", () => {
    const dummyArrayList = new ArrayList<number>();

    for (let i = 0; i < 10; i++) {
      dummyArrayList.addLast(i);
    }

    const sublist = dummyArrayList.subList(3, 5);

    expect(sublist.size()).toBe(5);
    expect(sublist.firstElement()).toBe(3);
    expect(sublist.lastElement()).toBe(7);
  });

  it("Is present should work properly with any type", () => {
    const dummyArrayList = new ArrayList<String>();

    dummyArrayList.addLast("JavaScript");
    dummyArrayList.addLast("TypeScript");
    dummyArrayList.addLast("PHP");
    dummyArrayList.addLast("GOLANG");

    expect(dummyArrayList.isPresent("JavaScript")).toBe(0);
    expect(dummyArrayList.isPresent("TypeScript")).toBe(1);
    expect(dummyArrayList.isPresent("PHP")).toBe(2);
    expect(dummyArrayList.isPresent("GOLANG")).toBe(dummyArrayList.size() - 1);
    expect(dummyArrayList.isPresent("PYTHON")).toBe(-1);
  });
});
