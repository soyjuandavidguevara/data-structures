import { LinkedList } from "../../datastructures/LinkedList";

describe("LinkedList", () => {
  it("Add elements should work (no position validation)", () => {
    const builtArrayToComparison = [];
    const dummyLinkedList = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      builtArrayToComparison.push(i);
      dummyLinkedList.addLast(i);
    }

    expect(builtArrayToComparison.length).toBe(dummyLinkedList.size());
  });

  it("Getting elements on LinkedList should work properly", () => {
    const dummyLinkedList = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      dummyLinkedList.addLast(i);
    }

    expect(dummyLinkedList.getElement(5)).toBe(5);
    expect(dummyLinkedList.firstElement()).toBe(0);
    expect(dummyLinkedList.lastElement()).toBe(9);
    expect(() => {
      dummyLinkedList.getElement(45);
    }).toThrow("Array indices out of range");

    const emptyArray = new LinkedList();

    expect(() => {
      emptyArray.firstElement();
    }).toThrow("Array indices out of range");
  });

  it("Adding elements on LinkedList should work properly", () => {
    const dummyLinkedList = new LinkedList<any>();

    expect(() => {
      dummyLinkedList.addElement("", 5);
    }).toThrow("Array indices out of range");

    dummyLinkedList.addElement("one", 0);
    expect(dummyLinkedList.firstElement()).toBe("one");

    dummyLinkedList.addElement("two", 0);
    expect(dummyLinkedList.firstElement()).toBe("two");

    dummyLinkedList.addElement("three", 0);
    expect(dummyLinkedList.firstElement()).toBe("three");

    expect(dummyLinkedList.lastElement()).toBe("one");

    dummyLinkedList.addElement("2.5", 1);

    expect(dummyLinkedList.getElement(1)).toBe("2.5");
  });

  it("Deleting elements on LinkedList should work properly", () => {
    const dummyLinkedList = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      dummyLinkedList.addLast(i);
    }

    expect(dummyLinkedList.deleteElement(5)).toBe(5);
    expect(dummyLinkedList.size()).toBe(9);
    expect(dummyLinkedList.deleteFirst()).toBe(0);
    expect(dummyLinkedList.size()).toBe(8);
    expect(dummyLinkedList.deleteLast()).toBe(9);
    expect(dummyLinkedList.size()).toBe(7);
    expect(dummyLinkedList.lastElement()).toBe(8);
  });

  it("ForEach function on LinkedList should work properly", () => {
    const dummyLinkedList = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      dummyLinkedList.addLast(i);
    }

    let count = 0;
    let dummyString = "";

    dummyLinkedList.forEach((e: any) => {
      count += e;
      dummyString += e.toString();
    });

    expect(count).toBe(45);
    expect(dummyString).toBe("0123456789");
  });

  it("SubList function on LinkedList should work properly", () => {
    const dummyLinkedList = new LinkedList<number>();

    for (let i = 0; i < 10; i++) {
      dummyLinkedList.addLast(i);
    }

    const sublist = dummyLinkedList.subList(3, 5);

    expect(sublist.size()).toBe(5);
    expect(sublist.firstElement()).toBe(3);
    expect(sublist.lastElement()).toBe(7);
  });
});
