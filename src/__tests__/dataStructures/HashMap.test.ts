import {HashMap} from "../../datastructures/HashMap";

describe("HashMap", () => {
    it("Should work rehash", () => {
       const dummyHashTable = new HashMap<number, number>(50);

       for (let i: number = 0; i <= 500000; i ++) {
           dummyHashTable.put(i, i*2);
       }

       expect(dummyHashTable.get(500000)).toBe(500000 * 2);
    });
});
