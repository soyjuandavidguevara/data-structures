import {MapEntry} from "../datastructures/MapEntry";

function defaultCompareFunction(key: any, element2: MapEntry<any, any>): number {
    if (key === element2.getKey()) {
        return 0;
    } else if (key > element2.getKey()) {
        return 1;
    } else {
        return -1;
    }
}

export abstract class Map<K, V> {
    public compareFunction: (e: K, e2: MapEntry<K, V>) => number;

    protected constructor(compareFunction?: (e1: K, e2: MapEntry<K, V>) => number) {
        this.compareFunction = compareFunction || defaultCompareFunction;
    }

    protected hash(key: string): number {
        let h = 0, i, chr;
        if (key.length === 0) return h;
        for (i = 0; i < key.length; i++) {
            chr = key.charCodeAt(i);
            h = ((h << 5) - h) + chr;
            h |= 0;
        }

        return h;
    }

}