function defaultCompareFunction(element1: any, element2: any) {
  if (element1 === element2) {
    return 0;
  } else if (element1 > element2) {
    return 1;
  } else {
    return -1;
  }
}

export abstract class List<E> {
  protected constructor(compareFunction?: (e1: any, e2: any) => number) {
    this.compareFunction = compareFunction || defaultCompareFunction;
  }

  public compareFunction: (e1: E, e2: E) => number;
  abstract firstElement(): E;
  abstract lastElement(): E;
  abstract getElement(position: number): E;
  abstract addFirst(element: E): void;
  abstract addLast(element: E): void;
  abstract addElement(element: E, position: number): void;
  abstract deleteFirst(): E;
  abstract deleteLast(): E;
  abstract deleteElement(position: number): E;
  abstract changeInfo(element: E, position: number): E;
  abstract forEach(callback: (e1: E) => any): void;
  abstract size(): number;
  abstract isEmpty(): boolean;
  abstract subList(position: number, numberElements: number): List<E>;
  abstract isPresent(element: any): number;

  exchangeInfo(position1: number, position2: number) {
    const element1 = this.getElement(position1);
    const element2 = this.getElement(position2);
    this.changeInfo(element1, position2);
    this.changeInfo(element2, position1);
  }
}
