# AlgsADT

AlgsADT is a JavaScript library for use algorithms and data structures like grapsh or queues.

## Installation

Use yarn or npm to install AlgsADT.

```bash
npm install algs-adt
```

## Usage

```typescript
import {List, ArrayList} from "algs-adt";

// Creates an ArrayList and LinkedList containing strings
const myAwesomeArrayList: List<string> = new ArrayList();
const myAwesomeLinkedList: List<string> = new LinkedList();

// Add an element
myAwesomeArrayList.addLast("Jhon Doe");
myAwesomeLinkedList.addLast("Jhon Doe");

// Returns 'Jhon Doe'
myAwesomeArrayList.firstElement();
myAwesomeLinkedList.firstElement();
```

## List Api
For any list you can use: addFirst(element), addLast(element), addElement(element, position), deleteFirst(): element, deleteLast(): element, deleteElement(position): element, fisrtElement(): element, lastElement(): element, getElement(position): element, size(): number, isEmpty(): boolean, changeInfo(element, position): element, subList(start, numberOfElements): List.
